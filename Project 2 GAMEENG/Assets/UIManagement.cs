﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagement : MonoBehaviour
{
    public GameObject gameOverUI;
    public Text scoreTxt;
    PlayerBase player;
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.onPlayerDead += GameOverActivate;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBase>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GameOverActivate()
    {
        gameOverUI.gameObject.SetActive(true);
        scoreTxt.text = "Final Score: " + player.points; 
    }
}
