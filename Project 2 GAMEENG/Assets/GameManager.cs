﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public PlayerBase player;
    public Text score;
    public GameObject[] tileRef;

    // Start is called before the first frame update
    void Start()
    {
        //PositionReset();

    }

    private void Awake()
    {
        PositionReset();
    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Score: " + player.points;
    }   

    void PositionReset()
    {
        Debug.Log("Tiles");
        GameObject[] tiles = GameObject.FindGameObjectsWithTag("Tile");
        foreach (GameObject a in tiles)
        {
            Debug.Log(a.GetComponent<Transform>().position);
        }
    }
}
