﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerometer : MonoBehaviour
{
    private Rigidbody rb;
    public bool isFlat = true;
    public float speed;
    public float forwardSpeed;
    public float jumpForce = 10.0f;
    public float gravity = 14.0f;
    private float verticalVelocity;
    float dirX;
    float dirY;
    MicInput micInput;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        micInput = GetComponent<MicInput>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 tilt = Input.acceleration;
        dirX = tilt.x * speed;
        dirY = tilt.y * speed;
        // Debug.Log("Y ACCELERATION" + dirY);
        Vector3 movement = Vector3.zero;
        movement.x = dirX;
        movement.y = verticalVelocity;
        movement.z = forwardSpeed;
 
        rb.velocity = movement * speed * Time.deltaTime;

        Vector3 jump = new Vector3(0, 5000, 0);
        if (micInput.GetMicrophoneLoudness() > -10.0f)  
        {
            Debug.Log(micInput.GetMicrophoneLoudness());
            verticalVelocity = jumpForce;
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }
      


        Debug.DrawRay(transform.position + Vector3.up, tilt, Color.cyan);
    }


    
}
