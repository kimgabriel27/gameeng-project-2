﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    PlayerBase player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBase>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerBase>() == player)
        {
            player.AddPoints(10);
            Destroy(this.gameObject);
        }
       
    }
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 100f, 0) * Time.deltaTime); //rotates 50 degrees per second around z axis
    }
}
