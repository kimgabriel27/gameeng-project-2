﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerBase : MonoBehaviour
{
    public SpawnManager spawnManager;

    bool isDead = false;

    public int points;
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.onPlayerDead += Die; //AddDynamic
    }

    void Update()
    {
        
    }


    public void Die()
    {
        isDead = true;
        this.GetComponent<MeshRenderer>().enabled = false;
        this.GetComponent<Rigidbody>().isKinematic = true;
        Debug.Log("Player Dead");
        //SceneManager.LoadScene(0);
    }

    public void AddPoints(int pointsToAdd)
    {
        points += pointsToAdd;
    }
}
