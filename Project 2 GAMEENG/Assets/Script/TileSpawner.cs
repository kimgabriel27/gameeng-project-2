﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    public GameObject tile;
    Vector3 nextSpawnPoint;
    public int initialTilesCount;
    GameObject groupParent;
    // Start is called before the first frame update
    void Start()
    {
        groupParent = this.gameObject;
        for(int i = 0; i < initialTilesCount; i++)
        {
            SpawnTile();
        }
    }

    private void Update()
    {
       
    }

    public void SpawnTile()
    {
        if(groupParent.transform.childCount > 0)
        {
            int lastChildIndex = groupParent.transform.childCount - 1;
            GameObject gameObject = Instantiate(tile, groupParent.transform.GetChild(lastChildIndex).transform.GetChild(3).transform.position, Quaternion.identity);
            gameObject.transform.parent = groupParent.transform;
        }
        else
        {
            GameObject i = Instantiate(tile, nextSpawnPoint, Quaternion.identity);
            i.transform.parent = groupParent.transform;
        }
    }



}
