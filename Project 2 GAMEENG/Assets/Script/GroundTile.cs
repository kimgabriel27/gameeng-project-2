﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class GroundTile : MonoBehaviour
{
    public Animator[] animator;
    TileSpawner tileSpawner;


    public GameObject obstaclePrefab;
    public GameObject lowFence;
    public GameObject coinPrefab;
    BoxCollider boxCollider;

    public float speed;
    Transform tileTransform;

    int number;
    // Start is called before the first frame update

    void Start()
    {
        GameEvents.current.onPlayerDead += StopMovement;
        tileTransform = GetComponent<Transform>();
        boxCollider = this.transform.GetChild(7).GetComponent<BoxCollider>(); 
        tileSpawner = GameObject.FindObjectOfType<TileSpawner>();
        number = Random.Range(0, 2);
        //Debug.Log(number);
        if ( number == 0)
        {
            SpawnObstacle(obstaclePrefab);
        }
        else if (number > 0)
        {
            SpawnObstacle(lowFence);
        }

        SpawnPickup();
      
    }

    // Update is called once per frame
    void Update()
    {
      transform.Translate(Vector3.back * Time.deltaTime * speed);
   
    }

    void SpawnObstacle(GameObject gameObject)
    {
        int obstacleSpawnIdx = Random.Range(4, 7);
        Transform spawnPoint = transform.GetChild(obstacleSpawnIdx).transform;

       GameObject a =  Instantiate(gameObject, spawnPoint.position, Quaternion.identity, transform);

        a.transform.parent = this.gameObject.transform;
    }


    //private void OnTriggerExit(Collider other)
    //{
    //    Debug.Log("OnEnter");
    //    tileSpawner.SpawnTile();
    //    //tileSpawner.RepositionTile(this.gameObject);
    //    Destroy(this.gameObject);
    //}

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnEnter");
        tileSpawner.SpawnTile();
        //tileSpawner.RepositionTile(this.gameObject);
        Destroy(this.gameObject);
    }


    void SpawnPickup()
    {
        Vector3 origin = boxCollider.transform.position;
        Vector3 range = boxCollider.transform.localScale / 2.0f;
       

        int randNum = Random.Range(0, 3);
        if(randNum > 0 )
        {
            for (int i = 0; i < randNum; i++)
            {
                Vector3 randomRange = new Vector3(Random.Range(-range.x, range.x), Random.Range(-range.y, range.y), Random.Range(-range.z, range.z));
                Vector3 randomPos = origin + randomRange;
                GameObject a = Instantiate(coinPrefab, randomPos, Quaternion.identity, transform);
                a.transform.parent = this.gameObject.transform; 
            }
        }
    }

    void StopMovement()
    {
        speed = 0;
    }

}
